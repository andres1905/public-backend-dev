# README #

## Endpoints ##

* POST http://localhost:8080/twist-biblioteca-backend/api/libros

```
#!json
{
    "isbn":"98765",
    "titulo":"Otro",
    "autor":"Autor",
    "anioPublicacion":2000,
    "resumen":"Resumen"
}
```

* GET http://localhost:8080/twist-biblioteca-backend/api/libros/12345
* GET http://localhost:8080/twist-biblioteca-backend/api/libros?titulo=Otro


## Funcionalidad Básica Implementada ##

### 01 - Registro de Libro ###

*Como usuario del sistema, yo quisiera poder registrar un libro.*

**Criterios de Aceptación**

Como usuario del sistema
Cuando intento registrar un libro con toda la información requerida
Entonces el libro es registrado exitosamente.

Como usuario del sistema
Cuando intento registrar un libro sin un título o ISBN
Entonces recibo un mensaje indicando que el valor faltante.

### 02 - Consulta de Libros ###

*Como usuario del sistema, yo quisiera poder consultar un libro registrado.*

**Criterios de Aceptación**

Como usuario del sistema
Cuando intento obtener información de un libro conociendo el ISBN
Entonces recibo la información del libro registrado.

Como usuario del sistema
Cuando intento obtener información de un libro conociendo el ISBN
Y el libro no se encuentra registrado
Entonces recibo un mensaje indicando que el libro no existe.


### 03 - Búsqueda de Libros ###

*Como usuario del sistema, yo quisiera poder buscar libros por título.*

**Criterios de Aceptación**

Como usuario del sistema
Cuando intento buscar un libro conociendo parte del título
Y existen libros registrados con títulos similares
Entonces recibo una lista de posibles coincidencias.

Como usuario del sistema
Cuando intento buscar un libro conociendo parte del título
Y no existen libros registrados con títulos similares
Entonces recibo un mensaje indicando este particular.
