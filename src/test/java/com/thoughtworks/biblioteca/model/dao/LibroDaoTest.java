package com.thoughtworks.biblioteca.model.dao;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.thoughtworks.biblioteca.model.entities.Libro;
import com.thoughtworks.biblioteca.util.LibroFactory;

@RunWith(MockitoJUnitRunner.class)
public class LibroDaoTest {

	@Mock private EntityManager em;
	
	@InjectMocks private LibroDao libroDao;
	
	@Test
	public void testRetornaLibroCuandoSeHaGuardado() {
		Libro libro = LibroFactory.crearLibro();
		
		when(em.merge(libro)).thenReturn(libro);
		
		Libro libroPersistido = libroDao.crear(libro);
		
		assertThat(libroPersistido, is(equalTo(libro)));
		verify(em).merge(libro);
	}
	
	@Test
	public void testRetornaLibroAlObtenerloPorIsbn() {
		Libro libro = LibroFactory.crearLibro();
		String isbn = libro.getIsbn();
		
		when(em.find(Libro.class, isbn)).thenReturn(libro);
		
		Libro libroObtenido = libroDao.obtenerPorIsbn(isbn);
		
		assertThat(libroObtenido, is(equalTo(libro)));
		verify(em).find(Libro.class, isbn);
	}
	
	@Test
	public void testRetornaLibrosAlObtenerlosPorTitulo() {
		Libro libro = LibroFactory.crearLibro();
		List<Libro> libros = new ArrayList<Libro>();
		libros.add(libro);
		
		TypedQuery<Libro> consulta = mock(TypedQuery.class);
		when(consulta.getResultList()).thenReturn(libros);
		when(em.createNamedQuery("Libro.buscarPorTitulo", Libro.class)).thenReturn(consulta);
		
		List<Libro> librosObtenidos = libroDao.obtenerPorTitulo(libro.getTitulo());
		
		assertThat(librosObtenidos, is(equalTo(libros)));
		verify(em).createNamedQuery("Libro.buscarPorTitulo", Libro.class);
		verify(consulta).setParameter("titulo", String.format("%%%s%%", libro.getTitulo()));
		verify(consulta).getResultList();
	}
}
