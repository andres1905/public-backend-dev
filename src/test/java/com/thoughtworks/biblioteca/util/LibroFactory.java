package com.thoughtworks.biblioteca.util;

import com.thoughtworks.biblioteca.model.entities.Libro;

public class LibroFactory {

	public static Libro crearLibro() {
		Libro libro = new Libro();
		libro.setIsbn("1234567890");
		libro.setAutor("Juan Perez");
		libro.setAnioPublicacion(2000);
		libro.setTitulo("Titulo del Libro");
		libro.setResumen("Un resumen corto del libro.");
		return libro;
	}
}
