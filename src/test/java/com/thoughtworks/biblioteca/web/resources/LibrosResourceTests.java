package com.thoughtworks.biblioteca.web.resources;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.StatusType;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.RuntimeDelegate;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.thoughtworks.biblioteca.model.dao.LibroDao;
import com.thoughtworks.biblioteca.model.entities.Libro;
import com.thoughtworks.biblioteca.util.LibroFactory;

@RunWith(MockitoJUnitRunner.class)
public class LibrosResourceTests {

	@Mock private UriInfo uriInfo;
	@Mock private LibroDao libroDao;
	@Mock private RuntimeDelegate runtimeDelegate;
	
	@InjectMocks private LibrosResource librosResource;
	
	private URI uri = URI.create("http://localhost/api/libros/00000");
	
	@Before
	public void setUp() {
		RuntimeDelegate.setInstance(runtimeDelegate);
	}
	
	@Test
	public void testRetornaResponseCuandoRegistroUnLibroConLaInformacionCompleta() {
		Libro libro = LibroFactory.crearLibro();
		
		UriBuilder uriBuilder = mock(UriBuilder.class);
	    ResponseBuilder responseBuilder = mock(ResponseBuilder.class);
	    Response expectedResponse = mock(Response.class);
	 
	    when(runtimeDelegate.createResponseBuilder()).thenReturn(responseBuilder);
	    when(runtimeDelegate.createUriBuilder()).thenReturn(uriBuilder);
	    when(uriInfo.getRequestUriBuilder()).thenReturn(uriBuilder);
	    when(uriBuilder.path(any(String.class))).thenReturn(uriBuilder);
	    when(uriBuilder.build()).thenReturn(uri);
	    when(Response.status(Response.Status.CREATED)).thenReturn(responseBuilder);
	    when(responseBuilder.status(Response.Status.CREATED)).thenReturn(responseBuilder);
	    when(responseBuilder.location(any(URI.class))).thenReturn(responseBuilder);
	    when(responseBuilder.entity(any(Libro.class))).thenReturn(responseBuilder);
	    when(responseBuilder.contentLocation(any(URI.class))).thenReturn(responseBuilder);
	    when(responseBuilder.build()).thenReturn(expectedResponse);
		
		Response response = librosResource.registrar(libro);
		
		assertThat(response, is(notNullValue()));
		verify(libroDao).crear(libro);
	}
	
	@Rule public ExpectedException libroSinIsbnException = ExpectedException.none();
	
	@Test
	public void testLanzaBadRequestExceptionCuandoRegistroUnLibroSinIsbn() {
		Libro libro = LibroFactory.crearLibro();
		libro.setIsbn(null);
		
		prepararBadRequestResponse();
		
		libroSinIsbnException.expect(BadRequestException.class);
		libroSinIsbnException.expectMessage(containsString("ISBN"));
		
		librosResource.registrar(libro);
	}
	
	@Rule public ExpectedException libroSinTituloException = ExpectedException.none();
	
	@Test
	public void testLanzaBadRequestExceptionCuandoRegistroUnLibroSinTitulo() {
		Libro libro = LibroFactory.crearLibro();
		libro.setTitulo(null);
		
		prepararBadRequestResponse();
		
		libroSinTituloException.expect(BadRequestException.class);
		libroSinTituloException.expectMessage(containsString("título"));
		
		librosResource.registrar(libro);
	}

	@Test
	public void testRetornaLibroCuandoSeConsultaPorIsbn() {
		Libro expectedLibro = LibroFactory.crearLibro();
		String isbn = expectedLibro.getIsbn();
		
		when(libroDao.obtenerPorIsbn(isbn)).thenReturn(expectedLibro);
		
		Libro libro = librosResource.obtener(isbn);
		
		assertThat(libro, is(equalTo(expectedLibro)));
		verify(libroDao).obtenerPorIsbn(isbn);
	}
	
	@Rule public ExpectedException libroNoEncontradoException = ExpectedException.none();
	
	@Test
	public void testLanzaNotFoundExceptionCuandoSeConsultaUnLibroPorIsbnYNoExiste() {
		String isbn = "12345";
		
		prepararNotFoundResponse();
		
		libroSinTituloException.expect(NotFoundException.class);
		libroSinTituloException.expectMessage(containsString(isbn));
		
		when(libroDao.obtenerPorIsbn(isbn)).thenReturn(null);
		
		librosResource.obtener(isbn);
	}
	
	@Test
	public void testRetornaLibrosCuandoSeConsultaPorTitulo() {
		Libro expectedLibro = LibroFactory.crearLibro();
		List<Libro> expectedLibros = new ArrayList<Libro>();
		expectedLibros.add(expectedLibro);
		
		String fragmentoTitulo = "Titulo";
		
		when(libroDao.obtenerPorTitulo(fragmentoTitulo)).thenReturn(expectedLibros);
		
		List<Libro> libros = librosResource.buscarPorTitulo(fragmentoTitulo);
		
		assertThat(libros, is(equalTo(expectedLibros)));
		verify(libroDao).obtenerPorTitulo(fragmentoTitulo);
	}
	
	@Rule public ExpectedException librosNoEncontradosException = ExpectedException.none();
	
	@Test
	public void testLanzaNotFoundExceptionCuandoSeConsultanLibrosPorTituloYNoHayCoincidencias() {
		String fragmentoTitulo = "Titulo";
		ArrayList<Libro> librosNoEncontrados = new ArrayList<Libro>();
		
		prepararNotFoundResponse();
		
		librosNoEncontradosException.expect(NotFoundException.class);
		librosNoEncontradosException.expectMessage(containsString(fragmentoTitulo));
		
		when(libroDao.obtenerPorTitulo(fragmentoTitulo)).thenReturn(librosNoEncontrados);
		
		librosResource.buscarPorTitulo(fragmentoTitulo);
	}
	
	private void prepararBadRequestResponse() {
		ResponseBuilder responseBuilder = mock(ResponseBuilder.class);
		Response expectedResponse = mock(Response.class);
		
		when(expectedResponse.getStatusInfo()).thenReturn((StatusType)Response.Status.BAD_REQUEST);
		when(runtimeDelegate.createResponseBuilder()).thenReturn(responseBuilder);
		when(Response.status(Response.Status.BAD_REQUEST)).thenReturn(responseBuilder);
		when(responseBuilder.status(Response.Status.BAD_REQUEST)).thenReturn(responseBuilder);
		when(responseBuilder.build()).thenReturn(expectedResponse);
	}
	
	private void prepararNotFoundResponse() {
		ResponseBuilder responseBuilder = mock(ResponseBuilder.class);
		Response expectedResponse = mock(Response.class);
		
		when(expectedResponse.getStatusInfo()).thenReturn((StatusType)Response.Status.NOT_FOUND);
		when(runtimeDelegate.createResponseBuilder()).thenReturn(responseBuilder);
		when(Response.status(Response.Status.NOT_FOUND)).thenReturn(responseBuilder);
		when(responseBuilder.status(Response.Status.NOT_FOUND)).thenReturn(responseBuilder);
		when(responseBuilder.build()).thenReturn(expectedResponse);
	}
}
