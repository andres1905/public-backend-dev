package com.thoughtworks.biblioteca.web.resources;

import java.net.URI;
import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.thoughtworks.biblioteca.model.dao.LibroDao;
import com.thoughtworks.biblioteca.model.entities.Libro;

@Path("/libros")
public class LibrosResource {

	@Context
	private UriInfo uriInfo;
	
	@EJB
	private LibroDao libroDao;

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response registrar(Libro libro) {
		if (libro.getIsbn() == null || libro.getIsbn() == "") {
			throw new BadRequestException("El libro que se intenta registrar no tiene asignado un ISBN.");
		}
		
		if (libro.getTitulo() == null || libro.getTitulo() == "") {
			throw new BadRequestException("El libro que se intenta registrar no tiene especificado un título.");
		}
		
		Libro libroCreado = libroDao.crear(libro);
		
		URI uriLibro = uriInfo.getRequestUriBuilder().path(libro.getIsbn()).build();
		return Response.created(uriLibro).entity(libroCreado).contentLocation(uriLibro).build();
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Libro obtener(@PathParam("id") String isbn) {
		Libro libro = libroDao.obtenerPorIsbn(isbn);
		
		if (libro == null) {
			throw new NotFoundException("No se ha encontrado ningún libro con ISBN '" + isbn + "'.");
		}
		
		return libro;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Libro> buscarPorTitulo(@QueryParam("titulo") String titulo) {
		List<Libro> libros = libroDao.obtenerPorTitulo(titulo);
		
		if (libros.isEmpty()) {
			throw new NotFoundException("No se ha encontrado ningún libro cuyo título coincida con '" + titulo + "'.");
		}
		
		return libros;
	}
}
